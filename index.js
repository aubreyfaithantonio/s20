let numVar = Number(prompt('Enter a number'));
console.log('The number you provided is ' + numVar)

for(numVar; numVar >= 0; numVar--){
	if(numVar <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if (numVar % 10 === 0){
		console.log('The number is divisible by 10. Skipping the number.')
		continue;
	} else if (numVar % 5 === 0){
		console.log(numVar)
	}
	
}

let superVar = "supercalifragilisticexpialidocious"
let consonants = ''

for(let i = 0; i <superVar.length; i++) {
	if (superVar[i] === "a" || superVar[i] === "e" || superVar[i] === "i" || superVar[i] === "o" || superVar[i] === "u"){
		continue;
	} else {
		consonants += superVar[i] 
	}
}

console.log(superVar);
console.log(consonants);
